import { WEKO_ATTRIBUTE_PARAM, WEKO_ATTRIBUTE_TEMPLATE_ID, joinPath } from "../../includes.ts";
import type { Constructor, SwapiPerson } from "../../types/types";
import {
  ATTRIBUTE_SW_BIRTH_YEAR,
  ATTRIBUTE_SW_EYE_COLOR,
  ATTRIBUTE_SW_GENDER,
  ATTRIBUTE_SW_HAIR_COLOR,
  ATTRIBUTE_SW_HEIGHT,
  ATTRIBUTE_SW_HOMEWORLD,
  ATTRIBUTE_SW_MASS,
  ATTRIBUTE_SW_NAME,
  ATTRIBUTE_SW_PATHNAME,
  ATTRIBUTE_SW_STARSHIPS,
  ATTRIBUTE_SW_UID,
  ATTRIBUTE_SW_VEHICLES,
  TAGNAME_SWAPI_CHARACTER,
  TAGNAME_SWAPI_CHARACTERS_ITEM
} from "../../constants/constants.ts";
import { type Action } from "../../mixins/Render/Render.ts";
import { BaseMixin, type BaseState } from "../../mixins/Base/Base.ts";
import { create } from "../../services/elementBuilder/elementBuilder.ts";

class Characters extends BaseMixin<Constructor, CharactersState>(HTMLElement) {
  #fetchStatus: { status: "active" | "complete" } | undefined;

  constructor() {
    super();
  }

  static get customTag(): string {
    return "swapi-characters";
  }

  static override get observedAttributes(): string[] {
    return [...(super.observedAttributes as string[])];
  }

  override beforeRender({ characters, routing }: CharactersState | undefined = {}): void {
    if (!characters || !routing) {
      this.#fetchData();
      return;
    }

    const actions: (() => void)[] = [
      ...this.#manageCharacter(routing?.childSegment, characters),
      ...this.#manageCharacterItems(routing?.childSegment, characters)
    ];

    if (actions.length) {
      this.setRenderAction("Characters_beforeRender", () => actions.forEach(action => action()));
    }
  }

  override segmentToModulePathname(segment: string | undefined): string[] {
    return ["/components/CharactersItem/CharactersItem.ts", "/components/Character/Character.ts"];
  }

  async #fetchData() {
    if (this.#fetchStatus) {
      return;
    }

    this.#fetchStatus = { status: "active" };

    const response = await fetch("https://swapi.info/api/people");
    if (this.#fetchStatus.status !== "active") {
      return;
    }

    this.#fetchStatus.status = "complete";

    if (!response || !response.ok) {
      throw Error("Failed to fetch people.");
    }

    /* swapi.dev */
    // const { next, previous, results } = await (response.json() as Promise<
    //   SwapiResponse<SwapiPerson>
    // >);
    /* swapi.info */
    const results = await (response.json() as Promise<Omit<SwapiPerson, "uid">[]>);

    // Add the `uid`.
    const characters = results.map<SwapiPerson>(c => {
      const characterUrl = new URL(c.url);
      const pathNameSegments = characterUrl.pathname.split("/");
      const uid = pathNameSegments.slice(-1)[0];
      return { ...c, uid };
    });

    this.setRenderState(current => ({ ...current, characters }));
  }

  #manageCharacter(childSegment?: string, characters?: SwapiPerson[]): Action[] {
    const character = characters?.find(c => c.uid === childSegment);
    const characterElement = document.body.querySelector(TAGNAME_SWAPI_CHARACTER);

    const actions: Action[] = [];

    if (this.#showWhich(childSegment) === "character") {
      // There should be a character element.
      if (character && characterElement) {
        // Existing character element but its contents need to be updated for a different character.
        actions.push(() =>
          this.setChildRoutingAttributes(
            { children: [characterElement] },
            { childSegment: childSegment as string }
          )
        );
      } else {
        // There is no character element yet, but we can only create one if we have the data to do
        // so.
        if (character) {
          console.log(
            "Characters.#manageCharacter: create character element; character=",
            character
          );
          const nextCharacter = create(TAGNAME_SWAPI_CHARACTER, {
            attributes: {
              [WEKO_ATTRIBUTE_TEMPLATE_ID]: "swapi-character",
              [WEKO_ATTRIBUTE_PARAM]: ":characterId",
              [ATTRIBUTE_SW_BIRTH_YEAR]: character.birth_year,
              [ATTRIBUTE_SW_EYE_COLOR]: character.eye_color,
              [ATTRIBUTE_SW_GENDER]: character.gender,
              [ATTRIBUTE_SW_HAIR_COLOR]: character.hair_color,
              [ATTRIBUTE_SW_HEIGHT]: character.height,
              [ATTRIBUTE_SW_HOMEWORLD]: character.homeworld,
              [ATTRIBUTE_SW_MASS]: character.mass,
              [ATTRIBUTE_SW_NAME]: character.name,
              [ATTRIBUTE_SW_UID]: character.uid
            },
            properties: {
              [ATTRIBUTE_SW_STARSHIPS]: character.starships,
              [ATTRIBUTE_SW_VEHICLES]: character.vehicles
            }
          });
          this.setChildRoutingAttributes(
            { children: [nextCharacter] },
            { childSegment: childSegment as string }
          );
          actions.push(() => {
            this.innerHTML = "";
            this.appendChild(nextCharacter);
          });
        }
      }
    } else {
      // There shouldn't be a character element.
      characterElement && actions.push(() => characterElement.remove());
    }

    return actions;
  }

  #manageCharacterItems(
    childSegment: string | undefined = undefined,
    characters?: SwapiPerson[]
  ): Action[] {
    const instance = this;
    const actions: Action[] = [];
    if (this.#showWhich(childSegment, characters) === "characterItems") {
      if (characters) {
        actions.push(() => (instance.innerHTML = ""));

        const characterItems = characters.map(item => {
          const characterId = item.url.split("/").slice(-1)[0];
          return create(TAGNAME_SWAPI_CHARACTERS_ITEM, {
            attributes: {
              [WEKO_ATTRIBUTE_TEMPLATE_ID]: "swapi-characters-item",
              [ATTRIBUTE_SW_HOMEWORLD]: item.homeworld,
              [ATTRIBUTE_SW_NAME]: item.name
              // [ATTRIBUTE_SW_PATHNAME]: joinPath(this._getChildPathname(), characterId)
            }
          });
        });

        if (characterItems.length) {
          actions.push(() => instance.append(...characterItems));
        }
      }
    } else {
      // Remove all the list items
      const characterItems = document.body.querySelectorAll(TAGNAME_SWAPI_CHARACTERS_ITEM);
      if (characterItems.length) {
        actions.push(() =>
          Array.from(characterItems)
            .reverse() // Faster to remove the children from the bottom right?
            .forEach(item => item.remove())
        );
      }
    }

    return actions;
  }

  #showWhich(
    childSegment?: string | undefined,
    characters?: SwapiPerson[]
  ): "character" | "characterItems" | void {
    if (childSegment) {
      return "character";
    }

    return !characters ? undefined : "characterItems";
  }
}

if (!window.customElements.get(Characters.customTag)) {
  window.customElements.define(Characters.customTag, Characters);
}

interface CharactersState extends BaseState {
  characters?: SwapiPerson[];
}
