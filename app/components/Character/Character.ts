import type { Constructor } from "../../types/types.ts";
import {
  ATTRIBUTE_SW_BIRTH_YEAR,
  ATTRIBUTE_SW_EYE_COLOR,
  ATTRIBUTE_SW_GENDER,
  ATTRIBUTE_SW_HAIR_COLOR,
  ATTRIBUTE_SW_HEIGHT,
  ATTRIBUTE_SW_HOMEWORLD,
  ATTRIBUTE_SW_MASS,
  ATTRIBUTE_SW_NAME,
  ATTRIBUTE_SW_UID,
  TAGNAME_SWAPI_CHARACTER
} from "../../constants/constants.ts";
import { BaseMixin, type BaseState } from "../../mixins/Base/Base.ts";
import { create } from "../../services/elementBuilder/elementBuilder.ts";

class Character extends BaseMixin<CharacterState, Constructor>(HTMLElement) {
  constructor(...args: any[]) {
    super(...args);
  }

  /************************************************************************************************
   * web component methods
   */
  // #region

  static override get observedAttributes(): string[] {
    return [
      ...(super.observedAttributes as string[]),
      ATTRIBUTE_SW_BIRTH_YEAR,
      ATTRIBUTE_SW_EYE_COLOR,
      ATTRIBUTE_SW_GENDER,
      ATTRIBUTE_SW_HAIR_COLOR,
      ATTRIBUTE_SW_HEIGHT,
      ATTRIBUTE_SW_HOMEWORLD,
      ATTRIBUTE_SW_MASS,
      ATTRIBUTE_SW_NAME,
      ATTRIBUTE_SW_UID
    ];
  }

  // #region

  /************************************************************************************************
   * Render
   */
  // #region

  override beforeRender(state: CharacterState | undefined): void {
    if (!state) {
      return;
    }

    const children = Array.from(this.children);

    let name: HTMLElement | undefined;
    if (!children.find(child => child.localName === "span")) {
      name = create("span", {
        properties: { slot: "name", textContent: state[ATTRIBUTE_SW_NAME] }
      });
    }

    name &&
      this.setRenderAction("Character_append", () => {
        name && this.append(name);
      });
  }

  // #endregion
}

if (!window.customElements.get(TAGNAME_SWAPI_CHARACTER)) {
  window.customElements.define(TAGNAME_SWAPI_CHARACTER, Character);
}

interface CharacterState extends BaseState {
  [ATTRIBUTE_SW_BIRTH_YEAR]: string;
  [ATTRIBUTE_SW_EYE_COLOR]: string;
  [ATTRIBUTE_SW_GENDER]: string;
  [ATTRIBUTE_SW_HAIR_COLOR]: string;
  [ATTRIBUTE_SW_HEIGHT]: string;
  [ATTRIBUTE_SW_HOMEWORLD]: string;
  [ATTRIBUTE_SW_MASS]: string;
  [ATTRIBUTE_SW_NAME]: string;
  [ATTRIBUTE_SW_UID]: string;
  "weko-segment": string;
}
