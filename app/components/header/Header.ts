import { BaseMixin } from "../../mixins/Base/Base.ts";

class Header extends BaseMixin(HTMLElement) {
  constructor() {
    super();
  }

  static get customTag(): string {
    return "ht-header";
  }

  override connected(): (() => void) | undefined {
    const anchors = this.querySelectorAll("nav a") as NodeListOf<HTMLAnchorElement>;

    const instance = this;
    function handleNavAnchorClick(this: Header, evt: Event): void {
      evt.preventDefault();
      const anchor = evt.target as HTMLAnchorElement;
      instance.pushState(anchor.getAttribute("href") || "");
    }

    anchors.forEach(link => {
      link.addEventListener("click", handleNavAnchorClick);
    });

    return () => {
      anchors.forEach(link => {
        link.removeEventListener("click", handleNavAnchorClick);
      });
    };
  }
}

if (!window.customElements.get(Header.customTag)) {
  window.customElements.define(Header.customTag, Header);
}
