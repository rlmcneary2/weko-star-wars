import {
  ATTRIBUTE_SW_HOMEWORLD,
  ATTRIBUTE_SW_NAME,
  ATTRIBUTE_SW_PATHNAME
} from "../../constants/constants.ts";
import { BaseMixin, type BaseState } from "../../mixins/Base/Base.ts";
import { create } from "../../services/elementBuilder/elementBuilder.ts";

class CharactersItem extends BaseMixin(HTMLElement) {
  constructor(...any: []) {
    super(...any);
  }

  static override get observedAttributes(): string[] {
    return [
      ...(super.observedAttributes as string[]),
      ATTRIBUTE_SW_HOMEWORLD,
      ATTRIBUTE_SW_NAME,
      ATTRIBUTE_SW_PATHNAME
    ];
  }

  /*
   * Weko API methods
   */
  // #region

  override beforeRender(state: CharactersItemState | undefined): void {
    if (!state) {
      return;
    }

    const instance = this;
    const children = Array.from(this.children);

    let name: HTMLElement | undefined;
    if (!children.find(child => child.localName === "a")) {
      name = create("a", {
        listeners: {
          click: (evt: MouseEvent) => {
            evt.preventDefault();
            instance.pushState(state[ATTRIBUTE_SW_PATHNAME]);
          }
        },
        properties: {
          href: state[ATTRIBUTE_SW_PATHNAME] ?? "",
          slot: "name",
          textContent: state[ATTRIBUTE_SW_NAME]
        }
      });
    }

    let homeworld: HTMLElement | undefined;
    if (!children.find(child => child.localName === "span")) {
      homeworld = create("span", {
        properties: {
          slot: "homeworld",
          textContent: state[ATTRIBUTE_SW_HOMEWORLD]
        }
      });
    }

    name &&
      homeworld &&
      this.setRenderAction("CharactersItem_append", () => {
        if (name && homeworld) {
          this.append(name, homeworld);
        }
      });
  }

  // #endregion
}

if (!window.customElements.get("swapi-characters-item")) {
  window.customElements.define("swapi-characters-item", CharactersItem);
}

interface CharactersItemState extends BaseState {
  [ATTRIBUTE_SW_HOMEWORLD]: string;
  [ATTRIBUTE_SW_NAME]: string;
  [ATTRIBUTE_SW_PATHNAME]: string;
}
