import {
  isWekoRoutingEvent,
  WEKO_ATTRIBUTE_PATHNAME,
  WEKO_ROUTING_EVENT_STATE_CHANGED,
  type WekoRoutingEventDetail,
  type WekoRoutingPathnameDataChanged
} from "../../includes.ts";
import type { Constructor } from "../../types/types.ts";

export function RenderMixin<S, T extends Constructor>(baseType: T) {
  return class Render extends baseType implements RenderType<S> {
    #doRender = false;
    #handleRoutingEventBound: ((evt: Event) => void) | undefined;
    #invokeRenderTimeout = 0;
    #pendingActions: Map<string, Action> = new Map();
    #queueForFrame: QueueForFrame | undefined;
    #state: S | undefined;

    constructor(...args: any[]) {
      super(...args);
    }

    /*
     * web component methods
     */
    // #region

    static get observedAttributes(): readonly string[] {
      // @ts-ignore
      return [...(super.observedAttributes ?? [])];
    }

    attributeChangedCallback(name: string, oldValue: unknown, newValue: unknown): void {
      // @ts-ignore
      super.attributeChangedCallback && super.attributeChangedCallback(name, oldValue, newValue);
      this.onUpdate({ name, type: "attribute", value: newValue });
    }

    connectedCallback(): void {
      this.#handleRoutingEventBound =
        this.#handleRoutingEventBound || this.#handleRoutingEvent.bind(this);
      window.addEventListener(WEKO_ROUTING_EVENT_STATE_CHANGED, this.#handleRoutingEventBound);

      // @ts-ignore
      super.connectedCallback && super.connectedCallback();

      this.onUpdate({ type: "connected" });

      // Trigger a render cycle because we were connected.
      setTimeout(() => {
        this.#doRender = true;
        this.#render();
      }, 0);
    }

    disconnectedCallback(): void {
      // @ts-ignore
      super.disconnectedCallback && super.disconnectedCallback();

      this.#handleRoutingEventBound &&
        window.removeEventListener(WEKO_ROUTING_EVENT_STATE_CHANGED, this.#handleRoutingEventBound);
      this.#handleRoutingEventBound = undefined;
    }

    // #endregion

    /**
     * API
     */
    // #region

    /**
     * Provide the callback that will be called with an action that will be invoked when it is time
     * to change the DOM.
     */
    set queueForFrameCallback(queueForFrame: QueueForFrame) {
      if (this.#queueForFrame !== queueForFrame) {
        this.#queueForFrame = queueForFrame;
        // this.#doRender &&
        //   console.log(
        //     `[${this.localName}]Render.queueForFrameCallback: invoke render; doRender=${
        //       this.#doRender
        //     } state=`,
        //     this.#state
        //   );
        this.#render();
      }
    }

    /**
     * Invoked based on state changes, before updating the DOM. Rendering involves two stages:
     * 1. In the body of `beforeRender` create elements or get values that will be set on elements,
     *    use one or more `setRenderAction` calls to provide callback functions.
     * 2. Inside those callback functions actually make changes to the DOM. These callback function
     *    will be invoked in an animation frame.
     * @param state The current state.
     */
    beforeRender(state: S | undefined): void {
      // Create elements and things...
      //
      // use `setRenderAction` to include changes to be made to the DOM.
    }

    /**
     *
     * @param current
     * @param next
     * @returns True if the current and next state are **not** the same.
     */
    changedState(current: S | undefined, next: S | undefined): boolean {
      return current !== next;
    }

    /**
     * Invoked when something notable happens that might require state being updated. Override and
     * implement to handle those updates. You probably want to use `setRenderState` to store data
     * and trigger a render cycle.
     * @param update Information about what's changed.
     */
    onUpdate(update: UpdateData | UpdateDataAttribute | UpdateDataPathname): void {
      // Override and if necessary update state.
    }

    /**
     * Call within the `beforeRender` method to set an action that will be invoked to update the
     * DOM.
     * @param id The identifier for the action, if an identifier already exists the existing action
     * will be replaced with the new action.
     * @param action This callback is where DOM manipulation is done. You should create elements and
     * prep outside this callback, limit changes in the callback to things that actually change the
     * rendered DOM.
     */
    setRenderAction(id: string, action: Action): void {
      if (this.#pendingActions.has(id)) {
        console.warn(
          `[${this.localName}]setAction: id '${id}' already exists; replacing existing action.`
        );
      }

      this.#pendingActions.delete(id);
      this.#pendingActions.set(id, action);
    }

    /**
     * Set a value in render state. This triggers a render cycle.
     * @param state Either a state value or update function. The update function will get the
     * current state as its argument.
     */
    setRenderState(state: S | ((current: S | undefined) => S)): void {
      this.#doRender = true;

      let nextState: S | undefined;
      if (isSetterFunction<S>(state)) {
        nextState = state(this.#state);
      } else {
        nextState = state;
      }

      if (!this.changedState(this.#state, nextState)) {
        return;
      }

      this.#state = nextState;

      if (this.#invokeRenderTimeout) {
        return;
      }

      const instance = this;
      this.#invokeRenderTimeout = setTimeout(() => {
        this.#invokeRenderTimeout = 0;
        // this.#doRender &&
        //   console.log(
        //     `[${this.localName}]Render.setRenderState: invoke render; doRender=${
        //       this.#doRender
        //     } state=`,
        //     this.#state
        //   );
        instance.#render.call(instance);
      }, 0);
    }

    // #endregion

    #handleRoutingEvent(evt: Event): void {
      if (!isWekoRoutingEvent(evt)) {
        return;
      }

      if (evt.detail.source !== this) {
        return;
      }

      console.log(`[${this.localName}]Render.#handleRoutingEvent: detail=`, evt.detail);

      this.onUpdate({ ...evt.detail, type: "pathname" } as UpdateDataPathname);
    }

    #render() {
      if (!this.#doRender) {
        return;
      }

      this.#doRender = false;

      this.beforeRender(this.#state);

      if (this.#pendingActions.size) {
        this.#queueForFrame &&
          this.#queueForFrame({
            action: () => {
              this.#pendingActions.forEach(action => action());
              this.#pendingActions.clear();
            }
          });
      }
    }
  };
}

function isSetterFunction<S>(obj: any): obj is (state: S) => S {
  return obj && typeof obj === "function";
}

export interface Action {
  (): void;
}

export interface QueueForFrame {
  (options: QueueOptions): Promise<void>;
}

export interface QueueOptions {
  action: () => void;
}

export interface RenderType<S> {
  queueForFrameCallback: QueueForFrame;
  beforeRender: (state: S | undefined) => void;
  onUpdate: (update: UpdateData | UpdateDataAttribute | UpdateDataPathname) => void;
  setRenderAction: (id: string, action: Action) => void;
  setRenderState: (state: S | ((current: S | undefined) => S)) => void;
}

export interface UpdateData {
  type: "attribute" | "connected" | "pathname";
}

export interface UpdateDataAttribute extends UpdateData {
  name: string;
  value: unknown;
  type: "attribute";
}

export interface UpdateDataPathname extends UpdateData, WekoRoutingPathnameDataChanged {
  type: "pathname";
}
