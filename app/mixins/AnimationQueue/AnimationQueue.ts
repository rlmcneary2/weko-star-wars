import type { Constructor } from "../../types/types.ts";

export function AnimationQueueMixin<T extends Constructor>(baseType: T) {
  class AnimationQueue extends baseType implements AnimationQueueMethods {
    #queue: QueueAction[] = [];
    #frameRequestId = 0;

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor(...args: any[]) {
      super(...args);
    }

    static get observedAttributes(): readonly string[] {
      // @ts-ignore
      return [...(super.observedAttributes ?? [])];
    }

    async queueForFrame(options: QueueAction): Promise<void> {
      this.#queue.push(options);

      if (this.#frameRequestId) {
        window.cancelAnimationFrame(this.#frameRequestId);
      }

      this.#frameRequestId = window.requestAnimationFrame(() => {
        this.#frameRequestId = 0;

        this.#queue.forEach(({ action }) => action());

        this.#queue.length = 0;
      });
    }
  }

  return AnimationQueue;
}

export interface AnimationQueueMethods {
  queueForFrame(options: QueueAction): Promise<void>;
}

export interface QueueAction {
  action: () => void;
}
