import type { Constructor } from "../../types/types.ts";
import {
  WEKO_CHANGED_EVENT,
  WekoLifecycleMixin,
  WekoRenderMixin,
  WekoRoutingMixin,
  isWekoChangedEvent,
  type WekoChangedEventAttributeDetails,
  type WekoChangedEventPathnameDetails
} from "../../includes.ts";

/**
 * This mixin function creates the `Base` class that "glues together" several weko mixins to create
 * a simpler way to create web components. Base class includes support for:
 * - routing
 * - rendering
 *
 * Ideally once your class extends using BaseMixin it will only need to implement
 * `observedAttributes` (from web components) and `beforeRender` (from RenderMixin).
 */
export function BaseMixin<T extends Constructor, S extends BaseState>(baseType: T) {
  /**
   * This class "glues together" the weko mixins to create a single base mixin that simpler web
   * components can be built from. It includes support for:
   * - routing
   * - rendering
   *
   * Ideally once your class extends using BaseMixin it will only need to implement
   * `observedAttributes` (from web components) and `beforeRender` (from RenderMixin).
   */
  class Base extends WekoLifecycleMixin(WekoRoutingMixin(WekoRenderMixin<T, S>(baseType))) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    constructor(...args: any[]) {
      // @ts-ignore
      super(...args);

      this.addEventListener(WEKO_CHANGED_EVENT, this.#handleWekoChangedEvent);
    }

    /*
     * overrides
     */
    // #region

    static override get observedAttributes(): string[] {
      return [...(super.observedAttributes ?? [])];
    }

    #handleWekoChangedEvent(evt: Event) {
      if (!isWekoChangedEvent(evt)) {
        return;
      }

      const { type } = evt.detail;

      console.log(`[${this.localName}]Base: event type='${type}', detail=`, evt.detail);

      switch (type) {
        case "attributes": {
          break;
        }

        case "connected": {
          break;
        }

        case "disconnected": {
          break;
        }

        case "pathname": {
          const { pathname: routing } = evt.detail as WekoChangedEventPathnameDetails;
          this.setRenderState(current => {
            const nextState = { ...(current || ({} as S)) };
            nextState.routing = routing;
            return nextState;
          });
          break;
        }
      }
    }
  }

  return Base;
}

export interface BaseState {
  routing?: WekoChangedEventPathnameDetails["pathname"];
}
