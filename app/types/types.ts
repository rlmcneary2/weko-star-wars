export type Constructor<T = HTMLElement> = new (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ...args: any[]
) => T;

export interface SwapiResponse<T> {
  next?: string;
  previous?: string;
  results: T[];
}

export interface SwapiPerson {
  birth_year: string;
  eye_color: string;
  gender: "male";
  hair_color: string;
  height: string;
  homeworld: string;
  mass: string;
  name: string;
  starships: string[];
  uid: string;
  url: string;
  vehicles: string[];
}
