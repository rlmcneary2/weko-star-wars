/**
 * This file allows us to toggle between using packages from `esm.sh` or locally installed packages
 * for development and testing.
 */
//export * from "https://esm.sh/@rlmcneary2/weko-routing@next";
// export * from "../node_modules/@rlmcneary2/weko-lifecycle/src/index.js";

//export * from "https://esm.sh/@rlmcneary2/weko-routing@next";
// export * from "../node_modules/@rlmcneary2/weko-routing/src/index.js";

import type {
  WekoChangedEventDetails as WekoChangedEventDetails_Lifecycle,
  WekoChangedEventAttributeDetails
} from "../node_modules/@rlmcneary2/weko-lifecycle/src/index.js";
import {
  WEKO_ATTRIBUTE_TEMPLATE_ID,
  WEKO_CHANGED_EVENT,
  WekoLifecycleMixin
} from "../node_modules/@rlmcneary2/weko-lifecycle/src/index.js";

import type {
  WekoChangedEventDetails as WekoChangedEventDetails_Routing,
  WekoChangedEventPathnameDetails
} from "../node_modules/@rlmcneary2/weko-routing/src/index.js";
import {
  WEKO_ATTRIBUTE_PARAM,
  joinPath,
  WekoRoutingMixin
} from "../node_modules/@rlmcneary2/weko-routing/src/index.js";

//export * from "https://esm.sh/@rlmcneary2/weko-render@next";
export * from "../node_modules/@rlmcneary2/weko-render/src/index.js";

interface WekoChangedEventDetails {
  type: WekoChangedEventDetails_Lifecycle["type"] | WekoChangedEventDetails_Routing["type"];
}

export type {
  WekoChangedEventDetails,
  WekoChangedEventAttributeDetails,
  WekoChangedEventPathnameDetails
};
export {
  WEKO_ATTRIBUTE_PARAM,
  WEKO_ATTRIBUTE_TEMPLATE_ID,
  WEKO_CHANGED_EVENT,
  joinPath,
  WekoLifecycleMixin,
  WekoRoutingMixin
};

export function isWekoChangedEvent(evt: any): evt is CustomEvent<WekoChangedEventDetails> {
  return evt && "detail" in evt && "type" in evt.detail;
}
