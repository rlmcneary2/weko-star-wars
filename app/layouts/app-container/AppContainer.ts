import type { Constructor } from "../../types/types";
import { BaseMixin, type BaseState } from "../../mixins/Base/Base.ts";

class AppContainer extends BaseMixin<Constructor, AppContainerState>(HTMLElement) {
  constructor() {
    super();
  }

  override segmentToModulePathname(segment: string | undefined): string[] {
    let modulePathname: string[] = [];

    switch (segment) {
      case "characters": {
        modulePathname.push("/pages/Characters/Characters.ts");
        break;
      }
    }

    return modulePathname;
  }

  override beforeRender(state: AppContainerState): void {
    console.log("AppContainer.beforeRender: enter, state=", state);
    if (!state) {
      return;
    }

    const childSegment = state?.routing?.childSegment;

    /** Used in the render action. */
    const removals: Element[] = [];
    let found: HTMLElement | undefined;
    for (let child of this.children) {
      const [, name] = child.tagName.toLowerCase().split("-");
      if (name === childSegment) {
        found = child as HTMLElement;
        continue;
      }

      removals.push(child);
    }

    /** Used in the render action. */
    let append: (() => void) | undefined;
    if (!found) {
      // We expect that there is a template for each child path.
      const template = document.querySelector(`#swapi-${childSegment}`) as HTMLTemplateElement;

      if (!template) {
        throw Error(
          `There is no template in the body that matches the query '#swapi-${childSegment}'.`
        );
      }

      // We need to set routing attributes on the child nodes.
      const clone = template.content.cloneNode(true) as DocumentFragment;
      this.setChildRoutingAttributes(clone, {
        childSegment: childSegment ?? "",
        filter: child => child.localName.startsWith("swapi")
      });

      append = () => this.append(clone);
    }

    (removals.length || append) &&
      this.setRenderAction("AppContainer_render", () => {
        removals.forEach(elem => elem.remove());
        append && append();
      });
  }
}

if (!window.customElements.get("app-container")) {
  window.customElements.define("app-container", AppContainer);
}

interface AppContainerState extends BaseState {}
